const express = require("express");
const path = require("path");
const app = express();
const fs = require("fs");
const { getCVByUserId, createOrUpdateCV } = require("./db");
const bodyParser = require("body-parser");
const session = require("./auth")(app);

//if (process.env.NODE_ENV !== "production") {
app.use(require("cors")({ origin: "*" }));
//}

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// called by the react app to get the cv in json format
app.get("/cv/:userid?", async (req, res) => {
  try {
    res.setHeader("Content-Type", "application/json");
    res.send(await getCVByUserId(req.params.userid));
  } catch (error) {
    res.status(500).send({ error });
  }
});

// this call returns the app
app.get("/app/:userid?", async function (req, res) {
  // convert urls like /app?userid=amarsh.anand@gmail.com -> /app/amarsh.anand@gmail.com
  if (req.path === "/app" && !!req.query.userid)
    return res.redirect(`${req.path}/${req.query.userid}`);
  // ensure that the user asked for is present
  if (!req.params.userid) return res.redirect("/home");
  res.sendFile(path.join(__dirname, "app", "index.html"));
});

// we serve all filess from the root dir
app.use(express.static(path.join(__dirname, ".")));

// create or update a CV
app.post("/cv", async (req, res) => {
  try {
    if (!req.user)
      return res.status(401).send({ status: "User not logged-in" });
    if (req.user.emails[0].value !== req.body.userid)
      return res.status(400).send({
        status: `Logged-in user ${req.user.emails[0].value} attempted updating ${req.body.userid}`,
      });
    const { _id, userid } = await createOrUpdateCV(req.body);
    res.send({ _id, userid });
  } catch (error) {
    res.status(500).send({ error });
  }
});

// if no path matches (including /), redirect to /home. note this is also applicable for POST calls
app.use("/", function (req, res) {
  res.redirect("/home");
});

const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`App listening on port ${PORT}`);
});

## How is this all working

- The CV is essentially a React app. It is present in `/cv-react-app` dir
- To serve the app, you need to do a `npm run build` from the `/cv-react-app` dir
  - Other than building the app, this command also `cp`s the `/cv-react-app/build` dir to `/app` dir
- The CV app is loaded by calling `/app/amarshanand`. This does the following:
  - The app itself gets loaded into the browser
  - Once loaded, the `CV.js` file in the App grabs the _userid_ (`amarshanand` in this case) from the url and calls `/cv/amarshanand`, whch returns the CV in a json format
- All bad urls (including `/`) get redirected to `/home`, which ssrves the static homepage
  - If the call for CV json fails (the user doesnt exist - say, `/app/elonmusk`), the call is redirected to `/home`

## How would it all work

So, loggin out of your CV doesnt log you out of Auth0. If we put a button for `Change User`, that may solve the problem. Rather, loggin out should always log you out.

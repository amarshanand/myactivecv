const mongoose = require("mongoose");
const Schema = mongoose.Schema;
if (process.env.NODE_ENV !== "production") {
  require("dotenv").config();
}

mongoose.set("strictQuery", false);
mongoose.connect(
  process.env.MONGODB_URI,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  },
  (err) => {
    if (err) {
      console.log(err);
    } else {
      console.log("Connected to MongoDB");
    }
  }
);

const cvSchema = new Schema({
  userid: Schema.Types.String,
  json: Schema.Types.Mixed,
});
const cvModel = mongoose.model("CV", cvSchema);

exports.getCVByUserId = async (userid) => {
  const { json } = await cvModel.findOne({ userid });
  return json;
};

exports.createOrUpdateCV = async ({ userid, json }) => {
  return await cvModel.findOneAndUpdate(
    { userid },
    { userid, json },
    { upsert: true, new: true }
  );
};

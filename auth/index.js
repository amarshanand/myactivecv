const expressSession = require("express-session");
const passport = require("passport");
const Auth0Strategy = require("passport-auth0");
const authRouter = require("./auth");

require("dotenv").config();

module.exports = function (app) {
  /**
   * Session Configuration
   */
  const session = {
    secret: process.env.SESSION_SECRET,
    cookie: {},
    resave: false,
    saveUninitialized: false,
  };

  if (app.get("env") === "production") {
    // Serve secure cookies, requires HTTPS
    session.cookie.secure = true;
    app.set("trust proxy", 1);
  }

  /**
   * Passport Configuration
   */
  const strategy = new Auth0Strategy(
    {
      domain: process.env.AUTH0_DOMAIN,
      clientID: process.env.AUTH0_CLIENT_ID,
      clientSecret: process.env.AUTH0_CLIENT_SECRET,
      callbackURL: process.env.AUTH0_CALLBACK_URL,
    },
    function (accessToken, refreshToken, extraParams, profile, done) {
      return done(null, profile);
    }
  );

  /**
   *  App Configuration
   */
  app.use(expressSession(session));

  passport.use(strategy);
  app.use(passport.initialize());
  app.use(passport.session());

  passport.serializeUser((user, done) => {
    done(null, user);
  });

  passport.deserializeUser((user, done) => {
    done(null, user);
  });

  app.use("/", authRouter);

  app.get("/user", (req, res, next) => {
    if (!req.user)
      return res.status(401).send({ status: "User not logged-in" });
    const { id, user_id, provider, locale, _raw, _json, ...profile } = req.user;
    res.send(profile);
  });
};

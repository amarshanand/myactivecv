const express = require("express");
const axios = require("axios");
const router = express.Router();
const passport = require("passport");
const querystring = require("querystring");
const { getCVByUserId, createOrUpdateCV } = require("../db");

require("dotenv").config();

/**
 * Routes Definitions
 */

router.get(
  "/login",
  passport.authenticate("auth0", {
    scope: "openid email profile",
  }),
  (req, res) => {
    res.redirect("/");
  }
);

router.get("/callback", (req, res, next) => {
  passport.authenticate("auth0", (err, user, info) => {
    if (err) {
      return next(err);
    }
    if (!user) {
      return res.redirect("/login");
    }
    req.logIn(user, async (err) => {
      if (err) {
        return next(err);
      }
      const returnTo = req.session.returnTo;
      delete req.session.returnTo;
      // ensure that this user exists in the db. if not, create a blank one
      try {
        await getCVByUserId(user.emails[0].value);
      } catch (e) {
        await createOrUpdateCV({ userid: user.emails[0].value, json: {} });
      } finally {
        res.redirect(returnTo || `/app/${user.emails[0].value}`);
      }
    });
  })(req, res, next);
});

router.get("/logout", (req, res) => {
  req.logOut(() => {
    const logoutURL = new URL(`https://${process.env.AUTH0_DOMAIN}/v2/logout`);
    const searchString = querystring.stringify({
      client_id: process.env.AUTH0_CLIENT_ID,
      returnTo: req.query.returnTo,
    });
    logoutURL.search = searchString;
    res.redirect(logoutURL);
  });
});

module.exports = router;

## About this App

This App by itself is a old-school React App (uses classes).
The app downloads without any data. `CV.js` grabs the last path element and loads the json (say, `~/app/amarsh` would load `amarsh.json`).
In ths way, when the App is served from the node server, we can find the respsctive json file to display the contents of the App.

> If you run the App for dev purposes, replace `CV.js` with `CV_offline.js`. Since you cant _login_ from the React App, you cant realy use authenticated features

import React from "react";
import { withStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import { JsonEditor } from "jsoneditor-react";
import "jsoneditor-react/es/editor.min.css";
import cv from "../CV";
const styles = {};

class Editor extends React.Component {
  render() {
    return (
      <JsonEditor
        value={this.props.json}
        onChange={(json) => cv().updateJson(json)}
      />
    );
  }
}

Editor.propTypes = {
  classes: PropTypes.object.isRequired,
  json: PropTypes.object.isRequired,
};

export default withStyles(styles)(Editor);

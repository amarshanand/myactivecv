import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Phone from "@material-ui/icons/Phone";
import Email from "@material-ui/icons/Email";
import Avatar from "@material-ui/core/Avatar";
import Chip from "@material-ui/core/Chip";
import Popover from "@material-ui/core/Popover";

import urlParams from "../util";

const styles = (theme) => ({
  contactButton: {
    position: "fixed",
    right: "1em",
    bottom: "1em",
    "@media(max-width: 780px)": {
      opacity: "0.8",
    },
    zIndex: 11,
  },
  contacts: {
    margin: theme.spacing.unit,
  },
  chip: {
    margin: theme.spacing.unit,
  },
});

class Contact extends React.Component {
  state = { showContact: false, anchorEl: null };

  handleClick = (event) => {
    this.setState(...this.state, {
      anchorEl: event.currentTarget,
      showContact: true,
    });
  };

  handleClose = (event) => {
    this.setState(...this.state, {
      anchorEl: null,
      showContact: false,
    });
  };

  sendMail = () => {
    window.location.href =
      "mailto:" +
      this.props.json.contact.email +
      "&subject=Interested in your CV";
    this.setState(...this.state, {
      showContact: false,
    });
  };

  phoneCall = () => {
    window.location.href = "tel:" + this.props.json.contact.mobile;
    this.setState(...this.state, {
      showContact: false,
    });
  };

  skypeCall = () => {
    window.location.href = "skype:" + this.props.json.contact.skype;
    this.setState(...this.state, {
      showContact: false,
    });
  };

  linkedIn = () => {
    window.open(this.props.json.contact.linkedin, "_blank");
    this.setState(...this.state, {
      showContact: false,
    });
  };

  render() {
    const { classes } = this.props;
    return (
      <div>
        <Button
          variant="fab"
          color="secondary"
          aria-label="Add"
          className={classes.contactButton}
        >
          <Phone onClick={this.handleClick} />
        </Button>
        <Popover
          open={this.state.showContact}
          onClose={this.handleClose}
          className={classes.contact}
          anchorEl={this.state.anchorEl}
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
          transformOrigin={{ vertical: "bottom", horizontal: "center" }}
        >
          <div className={classes.contacts}>
            {this.props.json.contact.email && (
              <div>
                <Chip
                  variant="outlined"
                  color="primary"
                  avatar={
                    <Avatar>
                      <Email />
                    </Avatar>
                  }
                  label={this.props.json.contact.email}
                  className={classes.chip}
                  onClick={this.sendMail}
                />
                <br />
              </div>
            )}
            {this.props.json.contact.mobile && (
              <div>
                <Chip
                  variant="outlined"
                  color="primary"
                  avatar={
                    <Avatar>
                      <Phone />
                    </Avatar>
                  }
                  label={this.props.json.contact.mobile}
                  className={classes.chip}
                  onClick={this.phoneCall}
                />
                <br />
              </div>
            )}
            {this.props.json.contact.skype && (
              <div>
                <Chip
                  variant="outlined"
                  color="primary"
                  avatar={<Avatar src="img/skype.png" />}
                  label={this.props.json.contact.skype}
                  className={classes.chip}
                  onClick={this.skypeCall}
                />
                <br />
              </div>
            )}
            {this.props.json.contact.linkedin && (
              <Chip
                variant="outlined"
                color="primary"
                avatar={<Avatar src="img/linkedin.png" />}
                label="LinkedIn"
                className={classes.chip}
                onClick={this.linkedIn}
              />
            )}
          </div>
        </Popover>
        {!!urlParams.printSettings && urlParams.printContacts === "true" && (
          <div className={"contact"}>
            <div>{this.props.json.contact.email}</div>
            <div>{this.props.json.contact.mobile}</div>
          </div>
        )}
      </div>
    );
  }
}

Contact.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Contact);

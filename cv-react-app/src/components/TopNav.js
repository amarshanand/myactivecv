import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Avatar from "@material-ui/core/Avatar";
import Tooltip from "@material-ui/core/Tooltip";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import Print from "@material-ui/icons/Print";
import Edit from "@material-ui/icons/Edit";
import Save from "@material-ui/icons/Save";
import Exit from "@material-ui/icons/ExitToApp";
import Download from "@material-ui/icons/GetApp";
import InputBase from "@material-ui/core/InputBase";

import PrintMenu from "./PrintMenu";
import urlParams from "../util";
import CV from "../CV";

const styles = {
  root: {
    flexGrow: 1,
  },
  flex: {
    flexGrow: 1,
  },
  iconRight: {
    right: "-0.5em",
  },
  avatar: {
    marginRight: "2rem",
    width: "1.5rem",
    height: "1.5rem",
    display: "inline-block",
    verticalAlign: "bottom",
  },
  import: {
    padding: "2px 0.5rem 2px 0.1rem",
    display: "inline-block",
    alignItems: "center",
    borderRadius: "4px",
    background: "rgba(255,255,255,0.25)",
  },
  input: {
    marginLeft: 8,
    flex: 1,
  },
  textField: {
    width: 300,
  },
};

// the url for the cv should be in the format <base>/amarsh, hence, amarsh should exist in db. if not, we redirect to /home
const userid = window.location.href.substring(
  window.location.href.lastIndexOf("/") + 1
);

async function getCVJson(userid) {
  try {
    const response = await fetch(`/cv/${userid}`);
    if (!response.ok) return null;
    const data = await response.json();
    if (Object.keys(data).length > 0) return data;
    return null;
  } catch (error) {
    return null;
  }
}

class TopNav extends React.Component {
  state = {
    showPrintMenu: false,
    anchorEl: null,
    user: null,
    cvJson: null,
  };

  importPanel(classes) {
    return (
      <div>
        <div className={classes.import}>
          <InputBase
            className={classes.input}
            placeholder="Email to import from"
            variant="outlined"
            onChange={async (e) => {
              const cvJson = await getCVJson(e.target.value);
              this.setState({ ...this.state, cvJson });
            }}
          />
        </div>
        <IconButton
          disabled={!this.state.cvJson}
          aria-haspopup="true"
          color="inherit"
          onClick={(e) => {}}
        >
          <Tooltip title={`Import ActiveCV for this email`}>
            <Download
              onClick={() => {
                const mergedJson = { ...CV().json, ...this.state.cvJson };
                CV().updateJson({ hideEditor: true });
                setTimeout(() => {
                  CV().updateJson(mergedJson);
                }, 1);
              }}
            />
          </Tooltip>
        </IconButton>
      </div>
    );
  }

  render() {
    const { classes, json, user, onSave } = this.props;
    return (
      <div>
        {urlParams.printSettings && (
          <Typography variant="title" color="inherit" className="CVtitle">
            {json.name}
          </Typography>
        )}
        <div className={classes.root + " topNav"}>
          <AppBar position="fixed">
            <Toolbar>
              {user && (
                <IconButton>
                  <Tooltip
                    title={`${user.displayName} is LoggedIn through ${user.emails[0].value}`}
                  >
                    <Avatar
                      alt={""}
                      src={user.picture}
                      className={classes.avatar}
                      style={{ margin: 0 }}
                      imgProps={{ referrerPolicy: "no-referrer" }}
                    />
                  </Tooltip>
                </IconButton>
              )}
              <Typography
                variant="title"
                color="inherit"
                className={classes.flex}
              >
                {json.name}
              </Typography>
              {!user && (
                <IconButton
                  disabled={user && user.emails[0].value !== userid}
                  className={classes.iconRight}
                  aria-haspopup="true"
                  color="inherit"
                  onClick={(e) => {
                    if (!user) window.location.href = `/login`;
                  }}
                >
                  <Tooltip title={`Login and Edit your CV`}>
                    <Edit />
                  </Tooltip>
                </IconButton>
              )}
              {user &&
                user.emails[0].value === userid &&
                this.importPanel(this.props.classes)}
              {user && user.emails[0].value === userid && (
                <IconButton
                  className={classes.iconRight}
                  aria-haspopup="true"
                  color="inherit"
                  onClick={onSave}
                >
                  <Tooltip title={`Save edits to your CV"}`}>
                    <Save />
                  </Tooltip>
                </IconButton>
              )}
              <IconButton
                className={classes.iconRight}
                aria-haspopup="true"
                color="inherit"
                onClick={(e) =>
                  this.setState(...this.state, {
                    anchorEl: e.currentTarget,
                    showPrintMenu: true,
                  })
                }
              >
                <Tooltip title={`Print this CV in a recruiter-friendly format`}>
                  <Print />
                </Tooltip>
              </IconButton>
              {user && (
                <IconButton
                  className={classes.iconRight}
                  aria-haspopup="true"
                  color="inherit"
                  onClick={(e) => {
                    this.setState({ user: null });
                    window.location.href = `/logout?returnTo=${window.location.href.replace(
                      "/app/",
                      "/app?userid="
                    )}`;
                  }}
                >
                  <Tooltip title={`Logout`}>
                    <Exit />
                  </Tooltip>
                </IconButton>
              )}
            </Toolbar>
          </AppBar>
          <PrintMenu
            showPrintMenu={this.state.showPrintMenu}
            anchorEl={this.state.anchorEl}
            onClose={() =>
              this.setState(...this.state, {
                anchorEl: null,
                showPrintMenu: false,
              })
            }
          />
        </div>
      </div>
    );
  }
}

TopNav.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TopNav);

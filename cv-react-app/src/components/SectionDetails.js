import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import Typography from "@material-ui/core/Typography";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import ReactPlayer from "react-player";
import urlParams from "../util";

const styles = (theme) => ({
  fullText: {
    letterSpacing: "0.5px",
    paddingTop: "1.25em",
    "@media(max-width: 780px)": {
      paddingRight: "2em",
    },
  },
  video: {
    margin: "1em auto",
    "@media(max-width: 780px)": {
      width: "auto !important",
      height: "auto !important",
    },
  },
  techStack: {
    position: "absolute",
    right: "1.5em",
    bottom: "1em",
    textAlign: "center",
  },
  techStack_img: {
    height: "2em",
    verticalAlign: "baseline",
    marginBottom: "0.5em",
  },
  samples: {
    margin: "1rem auto",
    display: "inline-flex",
    width: "100%",
    justifyContent: "space-evenly",
    flexWrap: "wrap",
    "@media(max-width: 780px)": {
      display: "block",
    },
  },
  container: {
    position: "relative",
    minWidth: "20rem",
    maxWidth: "20rem",
    "@media(max-width: 780px)": {
      margin: "0 auto",
    },
  },
  iframe: {
    width: "100%",
    marginBottom: "2rem",
    height: "20rem",
    border: "none",
    borderRadius: "4px",
    boxShadow: "0px 0px 5px 0px rgba(0,0,0,0.5)",
    "&:hover": {
      boxShadow: "0px 0px 10px 0px rgba(0,0,0,1)",
    },
  },
  open: {
    zIndex: 11,
    position: "absolute",
    top: "0.5rem",
    right: "0.5rem",
    width: "2rem",
    height: "2rem",
    background: "rgba(0,0,0,0.8) url(open_link.png) center no-repeat",
    backgroundSize: "70%",
    borderRadius: "6px",
    cursor: "pointer",
    boxShadow: "0px 0px 5px 0px rgba(0,0,0,0.5)",
  },
});

class SectionDetails extends React.Component {
  state = {
    value: urlParams.printSettings === "detailed" ? 1 : 0,
    label: urlParams.printSettings === "detailed" ? "details" : "summary",
  };

  handleChange = (event, value) => {
    this.setState({ value, label: event.target.textContent.toLowerCase() });
  };

  render() {
    const { classes } = this.props;

    const Techstack = (props) => {
      return (
        <div className={classes.techStack + " techStack"}>
          {props.techStack.map((tech) => (
            <div key={tech}>
              <img
                className={classes.techStack_img}
                src={`techstack/${tech}.png`}
                alt={tech}
                title={tech}
              />
            </div>
          ))}
        </div>
      );
    };

    const Samples = ({ urls }) => {
      return (
        <div className={classes.samples}>
          {urls.map((url, i) => {
            const isImage = url.match(/\.(jpeg|jpg|gif|png)$/) != null;
            // https://www.youtube.com/watch?v=qUed6Ug4Cgo -> https://www.youtube.com/embed/qUed6Ug4Cgo
            if (url.indexOf("youtube.com") > 0)
              url = "https://www.youtube.com/embed/" + url.split("=")[1];
            // https://drive.google.com/file/d/112QJiI2qNDb7ouxBR5Rj8rQUkxejQfy1/view?usp=sharing -> https://drive.google.com/file/d/112QJiI2qNDb7ouxBR5Rj8rQUkxejQfy1/preview
            if (url.indexOf(".google.com/") > 0)
              url = url
                .replace(/view\?usp=sharing/g, "preview")
                .replace(/edit\?usp=sharing/g, "preview");
            console.log(url);
            return (
              <div title={url} key={i} className={classes.container}>
                {isImage && (
                  <img alt={url} className={classes.iframe} src={url} />
                )}
                {!isImage && (
                  <iframe title={url} className={classes.iframe} src={url} />
                )}
              </div>
            );
          })}
        </div>
      );
    };

    return (
      <ExpansionPanelDetails style={{ display: "block", position: "relative" }}>
        <Tabs
          className="expansionPanelTabs"
          value={this.state.value}
          onChange={this.handleChange}
          indicatorColor="secondary"
          textColor="secondary"
          centered
        >
          <Tab label="Summary" />
          <Tab label="Details" />
          {this.props.video && <Tab label="Video" />}
          {this.props.samples && <Tab label="Samples" />}
        </Tabs>
        {this.state.label === "summary" && (
          <Typography
            container="div"
            className={classes.fullText + " fullText"}
            variant="body2"
          >
            <div dangerouslySetInnerHTML={{ __html: this.props.summary }} />
            <Techstack techStack={this.props.techStack} />
          </Typography>
        )}
        {this.state.label === "details" && (
          <Typography
            container="div"
            className={classes.fullText + " fullText"}
            variant="body2"
          >
            <div dangerouslySetInnerHTML={{ __html: this.props.fullText }} />
          </Typography>
        )}
        {this.state.label === "video" && (
          <ReactPlayer className={classes.video} url={this.props.video} />
        )}
        {this.state.label === "samples" && (
          <Samples urls={this.props.samples} />
        )}
      </ExpansionPanelDetails>
    );
  }
}

SectionDetails.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SectionDetails);

let json,
  user,
  status = "waiting";

const userid = window.location.href.substring(
  window.location.href.lastIndexOf("/") + 1
);

let callback = () => {};
const registerCallback = (_callback) => (callback = _callback);

const updateJson = (_json) => {
  json = _json;
  console.log(`setting JSON to`);
  console.dir(json);
  callback();
};

const save = (done) => {
  fetch("/cv", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      userid,
      json,
    }),
  })
    .then((res) => {
      if (res.ok) return res.json();
      done("error", "Could not save CV");
    })
    .then((data) => {
      done("success", "CV Saved");
    })
    .catch((error) => {
      done("error", "Could not save CV");
    });
};

// the url for the cv should be in the format <base>/amarsh, hence, amarsh should exist in db. if not, we redirect to /home;
fetch(`/cv/${userid}`)
  .then((res) => {
    if (res.ok) return res.json();
    alert(`Could not hit /cv/${userid}. Will redirect to /home`);
    window.location.href = "/home";
  })
  .then((data) => {
    json = data;
    status = "received";
    // now that the data is received, get the logged-in user if one is available
    fetch(`/user`)
      .then((response) => {
        if (response.ok) return response.json();
        callback();
      })
      .then((data) => {
        if (!data) return callback();
        user = data;
        callback();
      })
      .catch((error) => {
        console.log(`error in getting user: ${error}`);
        callback();
      });
  })
  .catch((error) => {
    alert(`Could not hit /cv/${userid}. Will refirect to /home`);
    window.location.href = "/home";
  });

const cv = () => ({ status, user, json, save, updateJson, registerCallback });

export default cv;

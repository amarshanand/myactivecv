import React from "react";
import ReactDOM from "react-dom";
import "@babel/polyfill";
import "./index.css";
import App from "./App";
import urlParams from "./util";

ReactDOM.render(<App />, document.getElementById("root"));

if (urlParams.printSettings) {
  document.documentElement.style.setProperty("overflow", "unset");
  document.body.classList.add("printing");
  window.onload = function () {
    setTimeout(() => window.print(), 1000);
  };
}

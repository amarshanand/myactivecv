import React, { Component } from "react";
import { MuiThemeProvider } from "@material-ui/core/styles";
import "./App.css";
import theme from "./theme";
import TopNav from "./components/TopNav";
import BodyPart from "./components/BodyPart";
import Contact from "./components/Contact";
import Editor from "./components/Editor";
import Split from "react-split-it";
import Snackbar from "@material-ui/core/Snackbar";
import CheckCircleOutlineIcon from "@material-ui/icons/CheckCircleOutline";
import ErrorOutlineIcon from "@material-ui/icons/ErrorOutline";
import "./example-split.css";
import CV from "./CV";
import urlParams from "./util";
import { Typography } from "@material-ui/core";

const userid = window.location.href.substring(
  window.location.href.lastIndexOf("/") + 1
);

class App extends Component {
  componentDidMount() {
    CV().registerCallback(() => this.forceUpdate());
  }

  render() {
    // display loading as we get the CV from the server ##### call registerCallback instead
    if (CV().status === "waiting")
      return (
        <div className="loading">
          <img src="img/loader.gif" alt="" />
        </div>
      );
    // if the user is logged-in, display the CV with the editor in a SplitPane
    if (
      CV().user &&
      CV().user.emails[0].value === userid &&
      !urlParams.printSettings
    )
      return (
        <MuiThemeProvider theme={theme}>
          <Split style={{ height: "calc(100vh + 4rem)" }}>
            <div className="content scrollable">
              <CVApp />
            </div>
            <div className="content scrollable" style={{ paddingTop: "4rem" }}>
              {!CV().json.hideEditor && <Editor json={CV().json} />}
            </div>
          </Split>
        </MuiThemeProvider>
      );
    // return the CV for public viewing
    return (
      <MuiThemeProvider theme={theme}>
        <CVApp />
      </MuiThemeProvider>
    );
  }
}

class CVApp extends Component {
  state = { notification: null };

  notificationJSX() {
    const { notification } = this.state;

    return (
      <Snackbar
        ContentProps={{
          style: {
            background:
              notification && notification.type === "error" ? "red" : "green",
          },
        }}
        open={!!notification}
        message={
          notification ? (
            <span>
              {notification && notification.type === "error" ? (
                <ErrorOutlineIcon className="notification-icon" />
              ) : (
                <CheckCircleOutlineIcon className="notification-icon" />
              )}
              {notification.text}
            </span>
          ) : null
        }
        autoHideDuration={5000}
        onClose={() => this.setState({ ...this.state, notification: null })}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "left",
        }}
      />
    );
  }

  render() {
    const json = CV().json;
    const user = CV().user;
    return (
      <div className={urlParams.printSettings ? "" : "appContainer"}>
        <TopNav
          json={json}
          user={user}
          onSave={() =>
            CV().save((type, text) =>
              this.setState({
                ...this.state,
                notification: { type, text },
              })
            )
          }
        />
        <div className="bodySpacer">
          {!json.body && (
            <Typography
              variant="title"
              style={{
                opacity: 0.6,
                marginTop: "calc(50vh - 4rem)",
                textAlign: "center",
              }}
            >
              Seems like your CV is empty.
              <br />
              <br />
              Start from someone else's CV. You just need to know their email.
            </Typography>
          )}
          {json.body &&
            json.body.map((bodyPart, index) => (
              <BodyPart
                key={index}
                title={bodyPart.title}
                type={bodyPart.type}
                sections={bodyPart.sections}
              />
            ))}
        </div>
        {json.contact && <Contact json={json} />}
        {this.notificationJSX()}
        <div className="fullCVMessage" style={{ opacity: 0 }}>
          View full CV at{" "}
          <u>
            <a href={window.location.origin}>{window.location.origin}</a>
          </u>
        </div>
      </div>
    );
  }
}

export default App;

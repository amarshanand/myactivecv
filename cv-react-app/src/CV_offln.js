let json = {}; //require("./sample_cv.json");

let callback = () => {};
const registerCallback = (_callback) => (callback = _callback);

const updateJson = (_json) => {
  json = _json;
  callback();
};

const cv = () => ({ status: "received", json, updateJson, registerCallback });

export default cv;
